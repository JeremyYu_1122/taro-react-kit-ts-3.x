/** @type {import('ts-jest/dist/types').InitialOptionsTsJest} */
module.exports = {
  preset: "ts-jest",
  testEnvironment: "node",
  verbose: true,
  moduleNameMapper: {
    "@tarojs/components": "@tarojs/components/dist-h5/react",
    "^.+\\.(css|scss|less)$": "<rootDir>/style-mock.js",
  },
  transform: {
    "^.+\\.(js|jsx|ts|tsx)$": "<rootDir>/node_modules/babel-jest",
    "^.+\\.esm.js?$": "ts-jest",
  },
  rootDir: __dirname,
  moduleFileExtensions: ["js", "jsx", "ts", "tsx", "json"],
  transformIgnorePatterns: [
    "<rootDir>/node_modules/(?!@taro)",
    "^.+\\.(css|sass|scss|less)$",
  ],
};
