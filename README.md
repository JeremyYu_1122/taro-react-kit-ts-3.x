# Taro 3.x 开发模板

## 环境

1. taro 版本：v3.x
2. typescript

### 命令

1. 安装依赖包：`yarn`

2. 开发小程序： `yarn dev:weapp`

3. 构建小程序： `yarn build:weapp`
