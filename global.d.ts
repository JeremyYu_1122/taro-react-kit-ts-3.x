/// <reference path="node_modules/@tarojs/plugin-platform-weapp/types/shims-weapp.d.ts" />

declare module "*.png";
declare module "*.gif";
declare module "*.jpg";
declare module "*.jpeg";
declare module "*.svg";
declare module "*.css";
declare module "*.less";
declare module "*.scss";
declare module "*.sass";
declare module "*.styl";

declare namespace NodeJS {
  interface ProcessEnv {
    TARO_ENV: "weapp" | "swan" | "alipay" | "h5" | "rn" | "tt" | "quickapp" | "qq" | "jd";
    NODE_ENV: string;
  }
}

/** 公共返回 */
type PublicApiResult<T> = {
  errCode: number;
  errMessage: string;
  success: boolean;
  data: T;
};

/** 公共分页返回 */
type PublicPageApiResult<T> = {
  total?: number;
} & PublicApiResult<T>;

type ResponseData<T> = {
  code: number;
  data: T;
  msg?: string;
};
