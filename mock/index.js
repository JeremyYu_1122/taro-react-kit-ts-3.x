const http = require("http");
const handleRule = require("./rule/template");

const app = http.createServer((req, res) => {
  res.setHeader("Access-Control-Allow-Origin", "*");
  res.setHeader("Access-Control-Allow-Method", "*");
  res.setHeader("Access-Control-Allow-Headers", "*");
  const data = handleRule(req.url);
  res.end(JSON.stringify(data));
});

const port = process.argv[2] || 10010;

app.listen(port, () => {
  console.log(`mock server running at ${port}`);
});
