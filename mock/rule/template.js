const { mock } = require("mockjs");

module.exports = function (url) {
  switch (url) {
    case "/hello":
      return mock({
        "list|10": {
          "id|+1": 1,
        },
      });
    default:
      return "not found";
  }
};
