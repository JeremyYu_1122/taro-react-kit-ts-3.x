/**
 * api数据转换层
 */

function getRule(rule: string) {
  switch (rule) {
    case "/hello":
      return {
        list: "testData",
      };
    default:
      return {};
  }
}

export function translateData<R extends Record<string, any>>(
  rule: string,
  data: Record<string, any>
): R {
  const ruleData = getRule(rule);
  const dataKeys = Object.keys(data);
  let newObj: any = {};
  for (let x of dataKeys) {
    if (ruleData[x]) {
      newObj[ruleData[x]] = data[x];
    } else {
      newObj[x] = data[x];
    }
  }
  return newObj;
}
