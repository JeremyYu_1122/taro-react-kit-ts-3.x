import { ERROR_LOGGER_KEY, ERROR_POST_URL } from "..//config";
import HandleError from "check-code-react";
import React from "react";

const CatchError = (Component: React.ComponentType) => (props: Record<string, any>) => {
  return (
    <HandleError loggerBaseUrl={ERROR_POST_URL} loggerKey={ERROR_LOGGER_KEY}>
      <Component {...props} />
    </HandleError>
  );
};

export default CatchError;
