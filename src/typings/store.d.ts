import { PayloadAction } from "@reduxjs/toolkit";

declare global {
  type sliceReducers<A, T extends keyof A> = {
    type: keyof A;
    payload: A[T] extends infer R ? R : never;
  };
}
