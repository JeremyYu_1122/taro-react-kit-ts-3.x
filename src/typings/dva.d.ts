import type { AnyAction } from "redux";
import type { ReducersMapObjectWithEnhancer, SubscriptionsMapObject } from "dva";

declare global {
  type decoratorFunction<T extends Record<string, any> = {}> = (
    target: T,
    propertyKey: string,
    descriptor: TypedPropertyDescriptor<any>
  ) => T | void;

  module Dva {
    type getDispatchResult<T extends Record<string, any> = {}> = (
      data: { type: keyof T } & T[keyof T]
    ) => void;

    interface IModel<S extends Record<string, any>, E extends AnyAction = { type: "" }> {
      namespace: string;
      state?: S;
      reducers?: IReducersMapObject<S> | ReducersMapObjectWithEnhancer;
      effects?: IEffectsMapObject<E>;
      subscriptions?: SubscriptionsMapObject;
    }

    type Reducer<S> = (state: S, action: AnyAction) => S;
    interface IReducersMapObject<S> {
      [key: string]: Reducer<S>;
    }

    type IEffect<T extends AnyAction> = (
      action: T,
      effects: IEffectsCommandMap<T>
    ) => void;
    type IEffectType = "takeEvery" | "takeLatest" | "watcher" | "throttle";
    type IEffectWithType<T extends AnyAction> = [IEffect<T>, { type: IEffectType }];
    interface IEffectsCommandMap<A> {
      put: (action: A) => A;
      call: Function;
      select: Function;
      take: Function;
      cancel: Function;
      [key: string]: any;
    }

    interface IEffectsMapObject<T extends AnyAction> {
      [key: string]: IEffect<T> | IEffectWithType<T>;
    }

    type DispatchState<T extends Record<string, any> = {}> = {
      dispatch: getDispatchResult<T>;
    };
  }
}
