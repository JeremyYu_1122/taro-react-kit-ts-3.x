import { useEffect } from "react";
import { View, Text } from "@tarojs/components";
import CatchError from "@/components/CatchError";
// import { testApi2 } from "@/api/template";
import "./index.scss";
import { useAppDispatch, useAppSelector } from "@/hook";
import { testAsyncUpdate, setA } from "@/store/template";
import { useTranslation } from "react-i18next";

function Index() {
  const state = useAppSelector((state) => state.template);
  const dispatch = useAppDispatch();
  const { t } = useTranslation();
  useEffect(() => {
    (async () => {
      dispatch(setA(123));
      dispatch(testAsyncUpdate(123));
    })();
  }, []);

  return (
    <View className="index">
      <Text>
        {t("hello")}!{state.a}
      </Text>
    </View>
  );
}

export default CatchError(Index);
