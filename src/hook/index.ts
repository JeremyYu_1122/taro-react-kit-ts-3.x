import { TypedUseSelectorHook, useDispatch, useSelector } from "react-redux";
import type { RootState, AppDispatch } from "../store";

export const useAppDispatch = <T extends AppDispatch>() => useDispatch<T>();
export const useAppSelector: TypedUseSelectorHook<RootState> = useSelector;

/** 获取token */
export async function useGetToken(isForce: boolean = false): Promise<string> {
  return "";
}
