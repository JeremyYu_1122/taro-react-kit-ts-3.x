/*
 * @Author: your name
 * @Date: 2020-03-05 10:41:02
 * @LastEditTime: 2021-05-21 15:06:09
 */

const nodeEnv = process.env.NODE_ENV as "development" | "production";

export const IS_PRODUCTION = process.env.NODE_ENV === "production";

/* 程序调用接口的前缀 */
export const REQUEST_URL = {
  development: "http://localhost:10010",
  production: "",
}[nodeEnv];

export const ERROR_POST_URL = {
  development: "http://localhost:8899",
  production: "",
}[nodeEnv];

export const ERROR_LOGGER_KEY = {
  development: "3f4d5fecd112789b7008ba8ad7874702",
  production: "",
}[nodeEnv];

/* cacheTTL失效时间 */
export const CACHE_EXPIRED_TIME = 24 * 60 * 60 * 1000 * 14;

/* RSA公钥 */
export const PUK = `-----BEGIN PUBLIC KEY-----
  XXXXXXXXXXXXXXX
  -----END PUBLIC KEY-----`;
