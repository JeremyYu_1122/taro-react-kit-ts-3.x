import i18n from "i18next";
import { initReactI18next } from "react-i18next";
import zh from "./index/zh.json";

i18n.use(initReactI18next).init({
  fallbackLng: "zh",
  lng: "zh",
  resources: {
    zh: {
      translation: zh,
    },
  },
});

export default i18n;
