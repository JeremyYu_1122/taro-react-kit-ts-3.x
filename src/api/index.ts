/*
 * @Author: your name
 * @Date: 2020-04-21 14:44:59
 * @LastEditTime: 2020-04-21 14:45:34
 * @LastEditors: Please set LastEditors
 * @Description: In User Settings Edit
 * @FilePath: /taro-kit/src/services/template.js
 */
import { ERROR_POST_URL } from "@/config";
import request from "../utils/request";

type loggerParam = {
  key: string;
  data: {
    type: "error" | "event" | "api";
    event: string;
    source: string;
    line: number;
    column: number;
    error: string;
  };
};

/**
 * 添加logger
 */
export async function addLogger(data: loggerParam) {
  return request(
    {
      url: "/logger/append",
      method: "POST",
      data,
    },
    { baseUrl: ERROR_POST_URL, isPrintErrLog: false }
  );
}
