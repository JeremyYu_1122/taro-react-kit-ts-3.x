/** 深拷贝 */
export function deepCloneObject<T>(obj: T): T {
  if (!obj) return obj;
  const keys = Object.keys(obj);
  const isArray = Object.prototype.toString.call(obj) === "[object Array]" ? true : false;
  const resultObj: any = isArray ? [] : {};
  keys.forEach((val) => {
    if (
      typeof obj[val] === "object" &&
      !(Object.prototype.toString.call(obj[val]) === "[object RegExp]")
    ) {
      resultObj[val] = deepCloneObject(obj[val]);
    } else {
      resultObj[val] = obj[val];
    }
  });
  return resultObj;
}

/** 通用防抖函数 */
type debounceParam = {
  func: Function;
};
export const debounce = function (during: number = 100) {
  let timer;
  return ({ func, ...data }: debounceParam) => {
    clearTimeout(timer);
    timer = setTimeout(() => {
      func(data);
    }, during);
  };
};

const alphabet = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnuvwxyz01234567890";

/** 获取一个随机数 */
export function getRandomCode(len: number) {
  let result: string[] = [];
  while (result.length < len) {
    const random = Math.ceil(Math.random() * alphabet.length);
    result.push(alphabet[random]);
  }
  return result.join("");
}

/** 数据结构转换 */
export function dataKeyTranslate<R extends any, T extends Record<string, any> = {}>(
  data: T | T[],
  translateObj: Partial<Record<keyof T, string>>
): R {
  const type = Object.prototype.toString.call(data);
  if (type === "[object Array]") {
    let tmp = data.map((val) => {
      let obj = {};
      for (let x in val) {
        obj[translateObj[x] || x] = val[x];
      }
      return obj;
    });
    return <R>tmp;
  } else if (type === "[object Object]") {
    let obj = {};
    for (let x in data) {
      (<Record<string, any>>obj)[translateObj[x] || x] = data[x];
    }
    return <R>obj;
  } else {
    return <R>data;
  }
}

export type OrderMethodData = {
  asc: boolean;
  col: string;
};

/** 对象数组转为get参数 */
export function translateGetMethodData(key: string, data: OrderMethodData[]) {
  if (!data.length) return "";
  let res = "";
  data.forEach((val, index) => {
    for (let x in val) {
      res += `${key}%5B${index}%5D.${x}=${val[x]}&`;
    }
  });
  res = res.substring(0, res.length - 1);
  return res;
}

/** 判断是否扫码进入小程序 */
export function checkIsScanEnter(scene: number) {
  switch (scene) {
    case 1011:
    case 1012:
    case 1013:
    case 1031:
    case 1032:
    case 1047:
    case 1048:
    case 1049:
      return true;
    default:
      return false;
  }
}

/** 格式化价格 */
export function formatPrice(price: string | number, splitNum: number = 2) {
  let res = "";
  let count = 0;
  let priceArr = String(price).replace(/\,/g, "").split(".");
  for (let i = priceArr[0].length - 1; i >= 0; i--) {
    if (count == splitNum && i !== 0) {
      count = 0;
      res += `${priceArr[0][i]},`;
    } else {
      count++;
      res += priceArr[0][i];
    }
  }

  return res.split("").reverse().join("") + (priceArr[1] ? `.${priceArr[1]}` : "");
}

/** 对象转为页面参数 */
export function objectToQuery(obj: Record<string, any>): string {
  let str = "";
  for (let x in obj) {
    str += `${x}=${obj[x]}&`;
  }
  str = str.substring(0, str.length - 1);

  return str;
}
