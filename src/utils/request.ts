import { ERROR_LOGGER_KEY, IS_PRODUCTION, REQUEST_URL } from "../config";
import Taro, {
  hideLoading,
  request as TaroRequest,
  showLoading,
  showModal,
  showToast,
} from "@tarojs/taro";
import { addLogger } from "@/api";
import { debounce, deepCloneObject, getRandomCode } from "./utils";
import { useGetToken } from "@/hook";

export interface IDefaultOptions {
  type?: "formData" | "json";
}
export interface IToggles {
  /**
   * 用于判断是否当 status >= 400 时的请求以通用toast的形式展示
   */
  isShowErrorToast?: boolean;
  /**
   * 是否显示console
   */
  isShowConsole?: boolean;
  /**
   * 超时时间
   */
  timeout?: number;
  /**
   * 是否添加token
   */
  isAddToken?: boolean;
  /**
   * 是否现实Loading
   */
  isShowLoading?: boolean;
  /**
   * baseUrl
   */
  baseUrl?: string;
  /**
   * 是否上传错误日志
   */
  isPrintErrLog?: boolean;
  /**
   * token过期是否重试
   */
  isAgain?: boolean;
  /**
   * 是否使用防抖方式请求
   */
  isDebuouce?: boolean;
  /**
   * 是否允许重复请求
   */
  agreeRepeat?: boolean;
  /**
   * 是否校验登录是否过期
   */
  isCheckExpire?: boolean;
}

type requestResponse<T> = {
  code?: number;
  errCode?: number;
  data: T;
  msg?: string;
  errMessage?: string;
  success?: boolean;
};

type requestTaskIdData = {
  /** 唯一Key */
  index: number;
  /** 请求结果 */
  next: Function;
  /** 请求参数 */
  options: TaroRequest.Option<any>;
  /** 请求配置 */
  toggles: IToggles;
  // 当前请求次数
  times: number;
};

function getRequestUrl(url: string, baseUrl: string = REQUEST_URL): string {
  return `${baseUrl}${url}`;
}
let requestParamList: TaroRequest.Option<any>[] = []; // 请求队列
let toggleList: IToggles[] = []; // 请求配置参数列表
let requestTaskId: Record<string, requestTaskIdData> = {}; //
let isGettingTask = false;
const MAX_REQUET_TIME = 3; // 最多重复请求次数
const requestDebounceFun = debounce(200);
/** 请求队列 */
async function requestDebounce(
  taskId: string,
  requestParams: TaroRequest.Option<any>,
  toggles: IToggles
): Promise<Taro.RequestTask<requestResponse<any>>> {
  return new Promise((resolve) => {
    const isSameRequest = requestParamList.some((val) => val.url === requestParams.url);

    // 允许重复请求或者不是重复的请求则插入请求队列
    if (!isSameRequest || toggles.agreeRepeat) {
      requestTaskId[taskId] = {
        index: requestParamList.length,
        options: requestParams,
        toggles,
        next: resolve,
        times: 1,
      };
      requestParamList.push(requestParams);
      toggleList.push(toggles);
    }
    if (isGettingTask) return;
    async function execFunc() {
      isGettingTask = true;

      try {
        const isShowLoading = toggleList.some((val) => val.isShowLoading);

        if (isShowLoading) {
          await showLoading({ title: "加载中", mask: true });
        }
        let execParam = requestParamList;
        if (toggles.isAddToken) {
          const token = await useGetToken();
          execParam = execParam.map((val) => {
            if (!val.header)
              val.header = {
                Authorization: token,
              };
            else if (val.header && !val.header.Authorization) {
              val.header.Authorization = token;
            }
            return val;
          });
        }

        const execArr = execParam.map((val) => () => TaroRequest(val));
        const execTaskId = deepCloneObject(requestTaskId);

        // 清除数据
        requestParamList = [];
        requestTaskId = {};
        toggleList = [];
        const result = await Promise.all(execArr.map((val) => val()));
        console.log("request result", result);
        // 结果分发到每个请求
        const taskIndexArr = Object.keys(execTaskId);
        for (let key of taskIndexArr) {
          const index = execTaskId[key].index;
          // 判断token是否已经过期
          const isExpire = await handleTokenExpire(requestParams, toggles, result[index]);
          // const isExpire = false;
          if (isExpire) {
            const newToken = await useGetToken();

            // 如果没有超过最大请求次数，再次插入
            if (execTaskId[key].times <= MAX_REQUET_TIME) {
              execTaskId[key].times++;
              let { options, toggles } = execTaskId[key];
              if (options.header && options.header.Authorization) {
                options.header.Authorization = newToken;
              } else {
                options.header = {
                  Authorization: newToken,
                };
              }
              requestParamList.push(execTaskId[key].options);
              requestTaskId = Object.assign(requestTaskId, execTaskId);
              toggleList.push(toggles);
            }
            break;
          } else {
            // 没有过期正常返回数据
            await execTaskId[key].next(result[index]);
          }
        }

        isGettingTask = false;
        if (isShowLoading) {
          await hideLoading();
        }
        if (requestParamList.length) {
          execFunc();
        }
      } catch (err) {
        console.log(err);
        isGettingTask = false;
        if (toggles.isShowLoading) {
          await hideLoading();
        }
        await showToast({ title: err.errMsg, icon: "none", mask: true });
      }
    }
    requestDebounceFun({
      func: execFunc,
    });
  });
}

const defaultToggles: IToggles = {
  isShowErrorToast: true,
  isShowConsole: IS_PRODUCTION ? false : true,
  isAddToken: true,
  isShowLoading: true,
  timeout: 120000,
  isPrintErrLog: false,
  isAgain: true,
  isDebuouce: true,
  isCheckExpire: true,
};
/** 处理token过期 */
async function handleTokenExpire(
  options: TaroRequest.Option<any>,
  toggles: IToggles,
  response: TaroRequest.SuccessCallbackResult<requestResponse<any>>
) {
  try {
    if (!response.data) return false;
    // token 重新获取
    if (
      (response.data.code === 40100 || response.data.errCode === 40100) &&
      toggles.isAgain
    ) {
      const token = await useGetToken(true);

      if (options.header && options.header.Authorization) {
        options.header.Authorization = token;
      } else {
        options.header = {
          Authorization: token,
        };
      }
      options.url = options.url.replace(toggles.baseUrl || REQUEST_URL, "");

      return true;
    }
  } catch {}

  return false;
}

/**
 * Requests a URL, returning a promise.
 */
export default async function request<T extends Record<string, any>>(
  options: TaroRequest.Option,
  toggles: IToggles = defaultToggles
): Promise<PublicPageApiResult<T>> {
  const { url } = options;
  const taskId = getRandomCode(16); // 请求队列ID

  toggles = deepCloneObject(Object.assign({}, defaultToggles, toggles));
  const { baseUrl, isPrintErrLog, isDebuouce } = toggles;

  const defaultOptions: TaroRequest.Option = {
    url: getRequestUrl(url, baseUrl),
    timeout: toggles.timeout,
  };

  const newOptions = Object.assign(options, defaultOptions);
  if (toggles.isAddToken && !toggles.isDebuouce) {
    const token = await useGetToken();
    if (!newOptions.header) {
      newOptions.header = {
        Authorization: token,
      };
    } else {
      newOptions.header.Authorization = token;
    }
  }
  try {
    const response = isDebuouce
      ? await requestDebounce(taskId, newOptions, toggles)
      : await TaroRequest(newOptions);

    let { statusCode, data } = response;

    if (statusCode === 200) {
      const result = response.data;

      if (typeof result === "object") {
        (<Record<string, any>>result).errMessage = decodeURI(
          result.errMessage || result.msg || ""
        );
        try {
          (<Record<string, any>>result).errMessage = JSON.parse(result.errMessage);

          if (
            result.errMessage.errors &&
            Object.prototype.toString.call(result.errMessage) === "[object Object]"
          ) {
            const errKeys = Object.keys(result.errMessage.errors);
            console.log(
              result.errMessage.errors[errKeys[0]],
              errKeys.length && Array.isArray(result.errMessage.errors[errKeys[0]])
            );

            if (errKeys.length && Array.isArray(result.errMessage.errors[errKeys[0]])) {
              result.errMessage = `${result.errMessage.errors[errKeys[0]][0] || ""}`;
            }
          } else {
            result.errMessage = result.errMessage.message || result.errMessage;
          }
        } catch {}
      }

      if (!result.success && isPrintErrLog) {
        addLogger({
          key: ERROR_LOGGER_KEY,
          data: {
            type: "api",
            event: "error",
            error: result.errMessage || result.msg || "",
            column: 0,
            line: 0,
            source: JSON.stringify(newOptions),
          },
        });
      }
      return result;
    }

    // 当接口返回 401 时，说明 token 过期、接口访问失败，需要登录
    return {
      errCode: 400,
      success: false,
      data: <any>{
        errMessage: data.errMessage || "",
      },
      errMessage: data.errMessage || "",
    };
  } catch (err) {
    console.log(err);

    if (isPrintErrLog) {
      addLogger({
        key: ERROR_LOGGER_KEY,
        data: {
          type: "api",
          event: "error",
          error: err,
          column: 0,
          line: 0,
          source: JSON.stringify(newOptions),
        },
      });
    }

    console.log("requst-catch", err);
    // 响应超时
    if (err && err.errMsg && err.errMsg === "request:fail timeout") {
      await showModal({
        title: "提示",
        content: "网络异常，请退出微信，检查网络后重新进入小程序",
        showCancel: false,
      });
    }
    return {
      errCode: 400,
      data: <T>{},
      errMessage: err,
      success: false,
    };
  }
}
