import { createSlice, SliceCaseReducers, createAsyncThunk } from "@reduxjs/toolkit";

type templateAction = {
  "template/setA": number;
  "template/setB": string;
};

export type TemplateState = {
  a: number;
  b: string;
};

interface TemplateReducers extends SliceCaseReducers<TemplateState> {
  setA: (
    state: TemplateState,
    action: sliceReducers<templateAction, "template/setA">
  ) => void;
  setB: (
    state: TemplateState,
    action: sliceReducers<templateAction, "template/setB">
  ) => void;
}

export const testAsyncUpdate = createAsyncThunk<number, {}, {}>(
  "asyncSetA",
  async (data: number) => {
    return new Promise((resolve) => {
      setTimeout(() => {
        resolve(data);
      }, 2000);
    });
  }
);

const sliceTemplate = createSlice<TemplateState, TemplateReducers, string>({
  name: "template",
  initialState: {
    a: 0,
    b: "",
  },
  reducers: {
    setA: (state, action) => {
      state.a = action.payload;
    },
    setB: (state, action) => {
      state.b = action.payload;
    },
  },
  extraReducers: (builder) => {
    builder.addCase(testAsyncUpdate.fulfilled, (state, { payload }) => {
      state.a = payload;
    });
  },
});

export const { setA, setB } = sliceTemplate.actions;

export default sliceTemplate.reducer;
