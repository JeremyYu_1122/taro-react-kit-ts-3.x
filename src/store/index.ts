import { configureStore } from "@reduxjs/toolkit";
import Template from "./template";

export const store = configureStore({
  reducer: {
    template: Template,
  },
});

export type RootState = ReturnType<typeof store.getState>;
export type AppDispatch = typeof store.dispatch;
